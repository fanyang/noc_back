module.exports = {
  apps : [
      {
        name        : "watch_all_service",
        script      : "./src/watch_process/index.js",
        interpreter : "babel-node",
        watch       : true,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production",
        }
      }
  ]
}