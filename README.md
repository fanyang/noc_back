## 安装服务器需要
    ```bash```
    apt-get install docker.io -y
    apt-get install docker-compose -y

## 配置节点rpc服务地址
    ```bash ```
    # 修改docker-compose, 更新数据位置
    environment:
      eos_node: 'http://47.75.200.188:8001'

## 后端应用启动
    ```bash```
    cd [project_path]
    docker-compose build && docker-compose down && docker-compose up -d

## 本机测试后端接口
    ```bash```
    curl http://127.0.0.1:3002/web/get_info




