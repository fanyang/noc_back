import {
  wait_time
} from '../utils/index'

import Eos from 'eosforcejs'

import config from '../config'

import db_models from '../db_manage/models'

import {
  update_or_create_model 
} from '../db_manage/models'

import {
  send_msg
} from '../mq_manage'

import rmq_config from './config'

const { info: info_model } = db_models;

const { eos_node } = config;

const EOS = Eos(eos_node);

const main = async () => {

  while(1){
    await wait_time(3);

    let latest_info = await info_model.findAll({order: [['id', 'desc']], limit: 1});
    latest_info = latest_info.length ? latest_info[0] : {};

    let is_info_error = false;
    let info = await EOS.getInfo({}).catch(err => {
      is_info_error = true;
    });

    if(is_info_error){
      continue;
    }

    let info_str = JSON.stringify(info),
        { head_block_num } = info;

    if(head_block_num != latest_info.head_block_num){
      console.log(`${latest_info.head_block_num}, ${head_block_num} ------------------->`);
    }else{
      continue ;
    }

    info.id = latest_info.id || 1;
    info.origin_data = info_str;
    await update_or_create_model('info', 'id', info);
    await send_msg({
      msg_type: 'exchange',
      exchange_name: rmq_config.top_info_exchange.name,
      msg: JSON.stringify({head_block_num: info.head_block_num, last_irreversible_block_num: info.last_irreversible_block_num})
    });
  }

}

main();