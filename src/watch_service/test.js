// import {
//   watch_action_for_service
// } from './watch_actions_for_service.js'

import Eos from 'eosjs'

const keyProvider ='5KYQvUwt6vMpLJxqg4jSQNkuRfktDHtYDp8LPoBpYo8emvS1GfG'

const node_config = {
  httpEndpoint: 'http://47.111.173.249:15001',
  chainId:'e2e441f0bd68c735f0f54f2ae9a0e783bb9d76368ba43b9eac486b97deca92b4',
  keyProvider
}

const { ecc, Fcbuffer } = Eos.modules;

const wait_time = async (t = 1) => {
  return new Promise((rs, rj) => {

    let w = setTimeout(() => {
      clearTimeout(w);
      rs();
    }, t * 1000);

  });
}

const get_random_num = (len = 2, is_arry = false, multtify_num = 8) => {
  let res = [];
  for(let i of new Array(len).keys()){
    let last_num = parseInt(Math.random() * 10000) % multtify_num + 1;
    res.push(last_num);
  }
  return !is_arry ? res.join('') : res;
}

const get_random_name = () => {
  let letters = '12345.abcdefghijklmnopqrstuvwxyz';
  let nums = get_random_num(12, true, letters.length - 1);
  let res = [];
  for(let p of nums){
    res.push( letters[p] )
  }
  return res.join('');
}

const get_public_key = (private_key) => {
  let public_key = ecc.privateToPublic(private_key).replace('EOS', '');
  return public_key;
}

const create_account = async (creator, newname, owner_key, active_key) => {
  let noc_system = await Eos(node_config).contract('bacc');
  let res = await noc_system.newaccount(creator, newname, owner_key, active_key, newname);
  return res;
}

const updateauth = async (actor, perm_name, parent_perm, public_key) => {
  let noc_system = await Eos(node_config).contract('bacc');
  let res = await noc_system.updateauth(actor, perm_name, parent_perm, public_key);
  return res;
}

// buy_cpu_net
const buy_cpu_net = async ( buyer, cpu_asset, net_asset ) => {
  let noc_system = await Eos(node_config).contract('bacc');
  let res = await noc_system.delegatebw(buyer, buyer, cpu_asset, net_asset, 0);
  console.log(res);
}

const transfer = async (from, to, quantity = '0.0000 BACC', memo = '') => {

  let res = await await Eos(node_config).transaction(['bacc.token'], token => {
    // token.bacc_token.transfer(from, to, quantity, memo);
    token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
    // token.bacc_token.transfer(from, to, quantity, memo + get_random_name(25));
  })

  // let token = await Eos(node_config).contract('bacc.token');
  // let res = await token.transfer(from, to, quantity, memo);
  console.log(res);
  return res;
}

const new_account_and_transfer = async (from, to, quantity = '0.0000 BACC', memo = '', owner_key = '', active_key = '') => {
  // let token = await Eos(node_config).contract('bacc.token');
  let eos = Eos(node_config);
  let bacc_abi = await eos.getAbi('bacc');
  // console.log(JSON.stringify(bacc_abi));

  let bacc_abi_token = await eos.getAbi('bacc.token');

  // console.log(JSON.stringify(bacc_abi_token));

  let perm = {actor: from, permission: "active"};
  // console.log(perm);

  let auth_permission = {authorization: `${from}@active`};
  // console.log(auth_permission);

  let res = await eos.transaction(['bacc.token', 'bacc'], (token) => {
    token.bacc.newaccount(from, to, owner_key, active_key, to, auth_permission);
    // token.bacc_token.transfer(from, to, quantity, memo, auth_permission);
    // token.bacc.updateauth('owner', )
  });
  

  return res;
}

// transfer
const test_transfer = async (from, to, quantity = '0.0000 BACC', memo = '') => {
  let public_key = get_public_key(node_config.keyProvider)
  // console.log( public_key );

  let public_key_accounts = await Eos(node_config).getKeyAccounts(`BACC${public_key}`)
  console.log( 'public_key_accounts', public_key_accounts );

  let info = await Eos(node_config).getInfo({});
  console.log(info);

  let token = await Eos(node_config).contract('bacc.token');
  while(true){
    let last_num = get_random_num(3);
    let ammount = `0.0${last_num} BACC`
    console.log(ammount);
    let random_to_name = public_key_accounts.account_names[ last_num % public_key_accounts.account_names.length ]
    console.log(random_to_name);
    let random_new_name = get_random_name()
    console.log(random_new_name);
    console.log(random_new_name.length);
    // let res = await create_account(from, random_new_name, public_key, public_key);
    let res = await token.transfer(from, random_to_name, ammount, memo);
    console.log(res);
    console.log('end_while----->');
    wait_time( 2 );
  }
}

const token_issue_and_create = async (to, quantity, memo) => {
  let token = await Eos(node_config).contract('bacc.token');
  await token.create(to, '10000000000.0000 ERT')
  let res = await token.issue(to, to, '1.0000 ERT', memo);
  return res;
}


const test_new_account = async (from, to, quantity = '0.0000 BACC', memo = '') => {
  let public_key = get_public_key(node_config.keyProvider)
  // console.log( public_key );

  let public_key_accounts = await Eos(node_config).getKeyAccounts(`BACC${public_key}`)
  console.log( 'public_key_accounts', public_key_accounts );

  let info = await Eos(node_config).getInfo({});
  console.log(info);

  let token = await Eos(node_config).contract('bacc.token');
  while(true){
    let last_num = get_random_num(3);
    let ammount = `0.0${last_num} BACC`
    console.log(ammount);
    let random_to_name = public_key_accounts.account_names[ last_num % public_key_accounts.account_names.length ]
    console.log(random_to_name);
    let random_new_name = get_random_name()
    console.log(random_new_name);
    console.log(random_new_name.length);
    let res = await new_account_and_transfer(from, random_new_name, ammount, ammount + '___multi_actions', public_key, public_key);

    // await token_issue(to, quantity, 'a test');
    let auth_permission = {authorization: `${from}@active`};
    // await updateauth(from, 'active', 'owner', public_key);
    // let res = await create_account(from, random_new_name, public_key, public_key);
    // let res = await transfer(from, random_new_name, ammount, ammount)
    // let res = await token.transfer(from, random_to_name, ammount, memo);
    console.log(res);
    console.log('end_while----->');
    await wait_time( 10 );
  }
}

const test_vote = async (voter, bpname, stake = '0.0000 BACC') => {
  let token = await Eos(node_config).contract('bacc');

  while(true){
    // await token.transaction({
    //   actions: [
    //     {
    //       account: "noc",
    //       name: "freeze",
    //       authorization: [
    //         {
    //           actor: voter,
    //           permission: "active"
    //         }
    //       ],
    //       data: {
    //         voter: voter,
    //         stake: stake_token
    //       }
    //     },
    //     {
    //       account: "noc",
    //       name: "vote",
    //       authorization: [
    //         {
    //           actor: voter,
    //           permission: "active"
    //         }
    //       ],
    //       data: {
    //         voter: voter,
    //         bpname: bpname,
    //         stake: stake_token
    //       }
    //     }
    //   ]
    // });

    // await token_issue_and_create(voter, '1.0000 BACC', 'cool');
    // await token.freeze(voter, stake);
    // await token.vote(vote, bpname, stake);
    // let t = await token.openbp('test.a', {authorization: `bacc@owner`});
    // console.log(t, 'is');
    let result = await Eos(node_config).transaction('bacc', tr => {
      tr.freeze(voter, stake);
      tr.vote(voter, bpname, stake);
      tr.votestatic(voter, bpname, stake, 1);
    });
    /*
    
      scope: voter,
      code: 'force',
      table: 'votes',
      json: true,
      limit: 1,

    */
    await Eos(node_config).getTableRows({
      'code': 'bacc',
      'scope': voter,
      'table': 'votes',
      'lower_bound': voter,
      'json': true
    })
    .then(data => {
      // console.log(JSON.stringify(data));
      console.log(data);
    })
    // console.log(JSON.stringify(result));
    console.log('vote end ----->')
    await wait_time( 4 );
  }
}

import {
  update_vote_by_account
} from './models/vote'
import {
  update_account_info
} from './models/accounts'

const main = async () => {
  test_vote('test.a', 'baccinitbpa', '10.0000 BACC');
  return ;
  update_account_info({
    account_name: ['wlfunlwk4lph', 'wlfunlwk4lph'],
    force: true
  })
  await buy_cpu_net('bacc.init', '100.0000 BACC', '100.0000 BACC')
  let last_num = get_random_num(3);
  while(1){
    test_new_account
    await Promise.all([
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
      ])
    await wait_time(0.1);
    await Promise.all([
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
        transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b'),    
      ])
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
    await transfer('bacc.init', 'wlfunlwk4lph', `0.1${ last_num } BACC`, 'from b');
    await wait_time(0.1);
  }
  
  // test_new_account('bacc.initbpq', 'bacc.dev', '0.0001 BACC');
  // test_vote('test.e', 'test.a', '10.0000 BACC');
  // update_vote_by_account('test.e')
  // baccceshi354
  
}

main();



// const test_action = {"receipt":{"receiver":"bacc","act_digest":"f36f4a1cfdf057d5e43fccef4e8a64fb7be5bcaebef47caeb0f2e5945f608e7f","global_sequence":23,"recv_sequence":12,"auth_sequence":[["test.a",1]],"code_sequence":1,"abi_sequence":1},"act":{"account":"bacc","name":"delegatebw","authorization":[{"actor":"test.a","permission":"active"}],"data":{"from":"test.a","receiver":"test.a","stake_net_quantity":"0.5000 BACC","stake_cpu_quantity":"0.5000 BACC","transfer":0},"hex_data":"000000001890b1ca000000001890b1ca881300000000000004424143430000008813000000000000044241434300000000"},"context_free":false,"elapsed":255,"console":"","trx_id":"5f25bf6662177ff1eba1ddc520e941e57bb74de4e55e0d773b63cc14a2a6ba16","block_num":13,"block_time":"2019-10-17T07:05:32.500","producer_block_id":"0000000d5611f7b1a108f7704a09e7526b7e09e4002dd415ae2b16cbef574a1e","account_ram_deltas":[{"account":"test.a","delta":544}],"except":null,"inline_traces":[{"receipt":{"receiver":"bacc.token","act_digest":"fe45d76d400a2c6ab329ccf3e204eb630119ded542af93af8d840a39c17176dc","global_sequence":24,"recv_sequence":12,"auth_sequence":[["test.a",2]],"code_sequence":1,"abi_sequence":1},"act":{"account":"bacc.token","name":"transfer","authorization":[{"actor":"test.a","permission":"active"}],"data":{"from":"test.a","to":"bacc","quantity":"1.0000 BACC","memo":"stake bandwidth"},"hex_data":"000000001890b1ca0000000000809039102700000000000004424143430000000f7374616b652062616e647769647468"},"context_free":false,"elapsed":158,"console":"","trx_id":"5f25bf6662177ff1eba1ddc520e941e57bb74de4e55e0d773b63cc14a2a6ba16","block_num":13,"block_time":"2019-10-17T07:05:32.500","producer_block_id":"0000000d5611f7b1a108f7704a09e7526b7e09e4002dd415ae2b16cbef574a1e","account_ram_deltas":[],"except":null,"inline_traces":[{"receipt":{"receiver":"test.a","act_digest":"fe45d76d400a2c6ab329ccf3e204eb630119ded542af93af8d840a39c17176dc","global_sequence":25,"recv_sequence":1,"auth_sequence":[["test.a",3]],"code_sequence":1,"abi_sequence":1},"act":{"account":"bacc.token","name":"transfer","authorization":[{"actor":"test.a","permission":"active"}],"data":{"from":"test.a","to":"bacc","quantity":"1.0000 BACC","memo":"stake bandwidth"},"hex_data":"000000001890b1ca0000000000809039102700000000000004424143430000000f7374616b652062616e647769647468"},"context_free":false,"elapsed":5,"console":"","trx_id":"5f25bf6662177ff1eba1ddc520e941e57bb74de4e55e0d773b63cc14a2a6ba16","block_num":13,"block_time":"2019-10-17T07:05:32.500","producer_block_id":"0000000d5611f7b1a108f7704a09e7526b7e09e4002dd415ae2b16cbef574a1e","account_ram_deltas":[],"except":null,"inline_traces":[]},{"receipt":{"receiver":"bacc","act_digest":"fe45d76d400a2c6ab329ccf3e204eb630119ded542af93af8d840a39c17176dc","global_sequence":26,"recv_sequence":13,"auth_sequence":[["test.a",4]],"code_sequence":1,"abi_sequence":1},"act":{"account":"bacc.token","name":"transfer","authorization":[{"actor":"test.a","permission":"active"}],"data":{"from":"test.a","to":"bacc","quantity":"1.0000 BACC","memo":"stake bandwidth"},"hex_data":"000000001890b1ca0000000000809039102700000000000004424143430000000f7374616b652062616e647769647468"},"context_free":false,"elapsed":21,"console":"","trx_id":"5f25bf6662177ff1eba1ddc520e941e57bb74de4e55e0d773b63cc14a2a6ba16","block_num":13,"block_time":"2019-10-17T07:05:32.500","producer_block_id":"0000000d5611f7b1a108f7704a09e7526b7e09e4002dd415ae2b16cbef574a1e","account_ram_deltas":[],"except":null,"inline_traces":[]}]}]};

// watch_action_for_service({
//   actions: [test_action]
// })
