import db_models from '../../db_manage/models'
import {
  excute_sql,
  update_or_create_model
} from '../../db_manage/models'

export const update_transaction_num = async () => {
  let count = await excute_sql('select count(*) as TRANSACTION_NUM  from actions');
  count = count[0].TRANSACTION_NUM;
  await update_or_create_model('analytics', 'key', {
    key: 'TRANSACTION_NUM',
    value: count
  })
}