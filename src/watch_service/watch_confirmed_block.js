
import db_models from '../db_manage/models'
import {
  processing_blocks 
} from './models/block'

import {
  consum,
  send_msg
} from '../mq_manage'

import rmq_config from './config'

// 同时接受多个
const block_ids = [];

consum({
  'bind_queue': rmq_config.top_info_exchange.confirm_block_queue,
  'msg_type': 'exchange',
  'exchange_name': rmq_config.top_info_exchange.name,
  'prefetch': 1,
  async call_back (msg, channel) {
    let start_time = new Date().getTime();
    let latest_ob = JSON.parse(msg.content.toString());

    const max_saved_key = 'max_saved_confirmed_block_num';
    let max_saved_block_num = await db_models.analytics.findAll({where: {key: max_saved_key}});
    max_saved_block_num = max_saved_block_num.length ? max_saved_block_num[0].value : 0;
    const {last_irreversible_block_num: listend_block_num} = latest_ob;

    if(listend_block_num < max_saved_block_num){
      console.log('head block num should > max_saved_block_num');
      channel.ack(msg);
      return ;
    }

    let keys = new Array(listend_block_num - max_saved_block_num),
        curl_block_nums = [];
    for(let item of keys.keys()){
      max_saved_block_num ++;
      curl_block_nums.push( {block_num:max_saved_block_num, is_confirmed: listend_block_num >= max_saved_block_num} );
    }

    try{
      await processing_blocks({block_nums: curl_block_nums, step: 40, save_key: max_saved_key, rmq_config: rmq_config.block_exchange_for_confirm});
      channel.ack(msg);
    }catch(error){
      channel.nack(msg, true);
      return ;
    }
    console.log(`block check takes ${new Date().getTime() - start_time}`)

  }
})