import Eos from 'eosforcejs'
import config from '../config'
import db_models from '../db_manage/models'


const { eos_node } = config;
export const EOS = Eos(eos_node);

import {
  query_available,
  update_users_token_available,
  update_account
} from './models/update_tables'

import {
  update_account_info,
  update_account_public_key,
  update_accounts_num,
  update_contracts_num,
  update_contracts_actions_by_account
} from './models/accounts'

import {
  consum,
  send_msg
} from '../mq_manage'

import {
  get_lastest,
  excute_sql,
  update_or_create_model
} from '../db_manage/models'

import {
  wait_time
} from '../utils/index'

import {
  get_value_by_type
} from './models/abi_utils'

import {
  update_transaction_num
} from './models/actions'

import {
  update_vote_by_account,
  update_vote_users_num
} from './models/vote'

import rmq_config from './config'


// update_account_public_key('lee123123123', 134314).then(data => {
//   console.log('test update_account_public_key -------> test');
// })
// .catch(error => {
//   console.log('error');
//   console.log('error in ------- update_account_public_key')
// });

const test_accout_public_key = async () => {
  // excute_sql
  let accounts = await excute_sql(`select distinct(name) from accounts`);
  let public_key_accounts = await excute_sql(`select distinct(name) from public_key_account`);

  for(let item of accounts){
    let pk = public_key_accounts.find(i => i.name == item.name);
    if(pk) continue;
    console.log(item);
    let create_block_num = await excute_sql(`select block_num from actions where name = 'newaccount' and origin_data like '%lee123123123%'`)
    create_block_num = create_block_num[0].block_num;
    console.log(create_block_num);
    await update_account_public_key(item.name, create_block_num).then(data => {
      console.log('test update_account_public_key -------> test');
    })
    .catch(error => {
      console.log('error');
      console.log('error in ------- update_account_public_key')
    });
  }
}

// test_accout_public_key();

// watched account_action
const operation_watch = {

  async 'bacc_newaccount' (action_item, action_account, action_name, action_data) {
    let {block_time: created, block_num: create_block_num} = action_item,
        {creator, name} = action_data;

    await update_or_create_model('accounts', 'name', {
      name,
      creator,
      created,
      create_block_num
    });

    await update_account_public_key(name, create_block_num);
    // update account num
    update_accounts_num();
  },

  async 'bacc_updateauth' (action_item, action_account, action_name, action_data) {
    let {block_time: created, block_num: create_block_num} = action_item,
        {creator, account: name} = action_data;

    await update_account_info({
      account_name: [name],
      force: true
    });

    await update_or_create_model('accounts', 'name', {
      name,
      creator,
      created,
      create_block_num
    });

    await update_account_public_key(name, create_block_num);

  },

  async 'bacc.token_issue' (action_item, action_account, action_name, action_data) {

  },

  async 'bacc_claim' (action_item, action_account, action_name, action_data) {

  },

  async 'bacc_vote' (action_item, action_account, action_name, action_data) {
    let {block_time: created, block_num: create_block_num} = action_item,
        {voter, account: name} = action_data;

    await update_vote_by_account(voter);
    await update_vote_users_num();
  },

  async 'bacc_votestatic' (action_item, action_account, action_name, action_data) {
    let {block_time: created, block_num: create_block_num} = action_item,
        {voter, account: name} = action_data;

    await update_vote_by_account(voter);
    await update_vote_users_num();
  },

  // update_vote_by_account

}

export const watch_action_for_service = async (action_res) => {
  let actions = [],
      invoed_users = [];

  for(let row of action_res.actions){
    let { block_num } = row,
        action_name = row.act.name,
        action_account = row.act.account,
        action_data = row.act.data,
        { memo } = action_data;


    let action_involved_users = await get_value_by_type(row, ['account_name', 'account_name[]']);
    action_involved_users = new Set(action_involved_users.map(i => i.value))
    action_involved_users.delete('');
    action_involved_users.add(action_account);
    action_involved_users = [...action_involved_users];
    // 全局，更新所有用户数据
    await update_account_info({
      account_name: action_involved_users
    });
    // UPDATE ACCOUNT_TOKEN FROM CHAIN
    let assets = await get_value_by_type(row, ['asset']);
    let token_types = assets.map(asset => {
      return asset.value.split(' ')[1]
    });
    token_types = [...new Set(token_types)];

    let update_account_token_proces = [];
    token_types.forEach(token_type => {
      update_account_token_proces.push(
          update_users_token_available( action_involved_users, token_type )
        )
    });

    await Promise.all(update_account_token_proces);
    
    // 更新交易数据总量
    update_transaction_num();

    update_contracts_num();

    update_contracts_actions_by_account(action_account);

    console.log(action_name, block_num);

    const operation_account_name = `${action_account}_${action_name}`;
    let contract_promise = [];
    if(operation_watch[operation_account_name]){
      // await operation_watch[operation_account_name](row, action_account, action_name, action_data);
      contract_promise.push( operation_watch[operation_account_name](row, action_account, action_name, action_data) );
    }
    if(contract_promise.length){
      await Promise.all(contract_promise);
    }

  }

}


const queues = rmq_config.action_exchange.action_queue;

const start_consum_channel = async () => {
  for(let queue of queues){
      console.log(queue);
      consum({
        'bind_queue': queue,
        'msg_type': 'exchange',
        'exchange_name': rmq_config.action_exchange.name,
        'prefetch': 1,
        async call_back (msg, channel) {
          console.log('start');
          let msg_ob = JSON.parse(msg.content.toString());
          try{
            await watch_action_for_service(msg_ob);
            channel.ack(msg);
          }catch(error){
            console.log(error);
            channel.nack(msg, true);
            return ;
          }
          
        }
      });
  }
}

const main = async () => {
  const channel_num = 6;
  for(let i of new Array(channel_num).keys()){
    start_consum_channel();
  }
}

main();





