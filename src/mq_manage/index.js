
import amqp from 'amqplib/callback_api'
import config from '../config'
import {
  wait_time
} from '../utils/index'

const {mq: mq_config} = config;

const QUEUE_EXCHANGE_DICT = {
  limit: 10,
  used: -1,
  connections: []
};

export const get_connection = async () => {
  let uri = `amqp://${mq_config.user}:${mq_config.password}@${mq_config.host}:${mq_config.port}/`;
  return new Promise(async (rs, rj) => {

    amqp.connect(uri, function(error0, connection) {
      // QUEUE_EXCHANGE_DICT.connections[_index] = connection;
      // QUEUE_EXCHANGE_DICT.used = _index;
      // console.log(`${QUEUE_EXCHANGE_DICT.connections.length} ${_index}------------===============>`);
      if (error0) {
        console.log(error0);
        rj(error0);
        return;
      }
      rs(connection);
    });
  });
}


export const create_channel = async (conn) => {
  return new Promise(async (rs, rj) => {
    conn = conn ? conn : await get_connection();
    conn.createConfirmChannel(function(error1, channel) {
      if (error1) {
        throw error1;
      }
      rs(channel);
    });
  });
}

export const create_queue = async (queue_name = '', channel) => {
  return new Promise((rs, rj) => {
    channel.assertQueue(queue_name, {
      durable: true
    }, function(error2, q) {
      if(error2){
        rj(error2);
        return;
      }
      rs(q);
    });
  });
}
/*
  msg_type: ['topic', 'queue', 'exchange']
*/
export const send_msg = async ({msg = '', name = 'queue_name', msg_type = 'queue', exchange_name = ''}) => {
  let conn = await get_connection();
  let channel = await create_channel(conn);
  if(msg_type == 'queue'){
    channel.assertQueue(name, {durable: true });
    channel.sendToQueue(name, Buffer.from(msg), {persistent: true});
    setTimeout(() => {
      conn.close();
    }, 1000);
  }

  if(msg_type == 'exchange'){
    channel.assertExchange(exchange_name, 'fanout', {durable: true });
    let start_time = new Date().getTime();
    channel.publish(exchange_name, '', Buffer.from(msg), [], () => {
      console.log('--------------> published')
      conn.close();
      console.log(`send msg takes ${new Date().getTime() - start_time}`)
    });
  }
}

export const consum = async ({bind_queue = '', exchange_name = '', prefetch = 1, msg_type = 'queue', call_back = () => {}}) => {
  let conn = await get_connection();
  let channel = await create_channel(conn);
  if(msg_type == 'queue'){
    channel.assertQueue(bind_queue, {durable: true });
    channel.prefetch(prefetch);
    channel.consume(bind_queue, function(msg) {
      call_back(msg, channel);
    }, {
      noAck: false
    });
  }

  if(msg_type == 'exchange'){
    let _q = await create_queue(bind_queue, channel);
    channel.assertExchange(exchange_name, 'fanout', {durable: true });
    channel.bindQueue(_q.queue, exchange_name, '');
    channel.prefetch(prefetch);
    channel.consume(_q.queue, function(msg) {
      call_back(msg, channel);
    }, {
      noAck: false
    });
  }

}

