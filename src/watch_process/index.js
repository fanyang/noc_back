console.log(process)
console.log(process.env.NODE_ENV)
import mysql from 'mysql'
import shell from 'shelljs'
import config from '../config'

const MYSQL_CONFIG = {
  user    : config.database.name,
  password: config.database.password,
  host    : config.database.host,
  port    : config.database.port || 3306,
  database: config.database.db_name
}

var connection = mysql.createConnection(MYSQL_CONFIG); 
connection.connect();

const query = (sql) => {
  return new Promise((rs, rj) => {

    connection.query(sql, '', (err, res) => {
      if(err){
        rj(err);
        return ;
      }
      rs(res);
    });

  });
}

const check_max_block_num = async () => {

  let sql = 'select id, block_num from  blocks order by id desc limit 1';

  let res = {
    max_block_num: -1,
    is_error: false,
    msg: ''
  }

  let query_res = await query(sql).catch(err => {
    res.is_error = true;
    res.msg = err;
  });

  if(res.is_error) return res;

  if(query_res && query_res.length && query_res[0] && query_res[0].block_num){
    res.max_block_num = query_res[0].block_num;
  }

  return res;
}

const wait_time =  (_time = 2) => {
  return new Promise((rs, rj) => {
    setTimeout(() => {
      rs();
    }, _time * 1000)
  })
}
// NODE_ENV=production nodemon src/index.js --exec babel-node
const main = async () => {
  let mem_max_block_num = -1;
  while(1){
    let {max_block_num, is_error, msg} = await check_max_block_num();

    if(is_error){
      console.log(msg);
      await wait_time(30);
      continue;
    }

    if(mem_max_block_num < 0){
      mem_max_block_num = max_block_num;
      await wait_time(30);
      continue;
    }

    if(mem_max_block_num == max_block_num){
      console.log(`mem_max_block_num is ${mem_max_block_num}, max_block_num is ${max_block_num}`)
      shell.exec('pm2 restart watch_info watch_confirmed_block watch_block watch_action_for_service');
      await wait_time(60);
      continue ;
    }

    console.log(`all is right: mem_max_block_num is ${mem_max_block_num}, max_block_num is ${max_block_num} `);
    mem_max_block_num = max_block_num;
    await wait_time(30);
  }
}

main();

