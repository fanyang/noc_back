import Express from 'express'
import cluster from 'cluster'
import os from 'os'
import index_route from './controller/explorer_ctr.js'
import activity_route from './controller/activity_ctr.js'
import contract_account from './controller/contract_account_ctr.js'
import account_token_ctr from './controller/account_token_ctr.js'
const numCPUs = 1;
const port = process.env.NODE_PORT || 3003;

var app = Express();
app.use(Express.json());
app.use('/', index_route);
app.use('/', activity_route);
app.use('/', contract_account);
app.use('/', account_token_ctr);

app.set('port',port);

console.log( numCPUs );
console.log(port);
app.listen(app.get('port'),function() {});
