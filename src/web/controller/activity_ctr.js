import Express from 'express'
import {
    get_last_item
} from '../../db_manage/models'
import { 
    get_vote_user_num,
    get_transaction_total,
    get_bps_from_db
} from '../models'

import {
    sync_data_api
} from '../settings/common_eos_api.js'


var router = Express.Router();

router.get('/web/get_info', (req, res) => {
  res.send({
      status: 0,
      data: 'this is info'
  })
});

router.post('/web/get_bps', (req, res) => {
    get_bps_from_db()
    .then(data => {
        res.send({
            status: 0,
            data: data
        })
    });
});

router.post('/web/get_last_info', (req, res) => {
    get_last_item('info')
    .then(data => {
        res.send({
            status: 0,
            data
        })
    })
});

router.post('/web/get_vote_user_num', (req, res) => {
    get_vote_user_num()
    .then(data => {
        res.send({
            status: 0,
            data
        });
    })
});

router.post('/web/get_transaction_total', (req, res) => {
    // let data = await get_transaction_total();
    get_transaction_total()
    .then(data => {
       res.send({
            status: 0,
            data
        }); 
   });
    
});

export default router;
