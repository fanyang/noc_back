import Sequelize from 'sequelize'

export default [
  // table info
  {
    keys: {
      server_version :  Sequelize.STRING ,
      chain_id :  Sequelize.STRING ,
      head_block_num :  Sequelize.INTEGER ,
      last_irreversible_block_num :  Sequelize.INTEGER ,
      last_irreversible_block_id :  Sequelize.STRING ,
      head_block_id :  Sequelize.STRING ,
      head_block_time :  Sequelize.STRING ,
      head_block_producer :  Sequelize.STRING ,
      virtual_block_cpu_limit :  Sequelize.DOUBLE ,
      virtual_block_net_limit :  Sequelize.DOUBLE ,
      block_cpu_limit :  Sequelize.DOUBLE ,
      block_net_limit :  Sequelize.DOUBLE ,
      server_version_string :  Sequelize.STRING ,
      origin_data: Sequelize.STRING
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'info'
    }
  },
  // table analytics
  {
    keys: {
      key: Sequelize.STRING,
      value: Sequelize.STRING
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'analytics'
    }
  },
  //table blocks
  {
    keys: {
      timestamp : Sequelize.DATE,
      producer  : Sequelize.STRING,
      confirmed : Sequelize.INTEGER,
      previous  : Sequelize.STRING,
      transaction_length: Sequelize.INTEGER,
      transaction_mroot : Sequelize.STRING,
      action_mroot : Sequelize.STRING,
      schedule_version : Sequelize.INTEGER,
      new_producers : Sequelize.STRING,
      header_extensions : Sequelize.STRING,
      producer_signature : Sequelize.STRING,
      transactions : Sequelize.STRING,
      block_extensions : Sequelize.STRING,
      block_id : Sequelize.STRING,
      block_num : Sequelize.INTEGER,
      ref_block_prefix : Sequelize.INTEGER,
      is_confirmed : Sequelize.BOOLEAN,
      origin_data: Sequelize.STRING
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'blocks'
    }
  },
  // table transactions
  {
    keys: {
      account : Sequelize.STRING,
      name : Sequelize.STRING,
      block_time : Sequelize.DATE,
      trx_id : Sequelize.STRING,

      _from : Sequelize.STRING,
      to : Sequelize.STRING,
      bpname : Sequelize.STRING,
      quantity : Sequelize.DOUBLE,
      stake : Sequelize.DOUBLE,
      voter : Sequelize.STRING,
      token_type : Sequelize.STRING,
      memo : Sequelize.STRING,
      
      actor : Sequelize.STRING,
      block_num : Sequelize.INTEGER,
      origin_data : Sequelize.STRING,
      permission : Sequelize.STRING,
      global_sequence : Sequelize.INTEGER,
      is_confirmed : Sequelize.INTEGER
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'actions'
    }
  },
  // accounts
  {
    keys: {
      name: Sequelize.STRING,
      available: Sequelize.DOUBLE,
      created: Sequelize.DATE,
      last_code_update: Sequelize.DATE,
      unlock_time: Sequelize.DATE,
      staked: Sequelize.DOUBLE,
      unstaking: Sequelize.DOUBLE,
      last_update_time: Sequelize.DATE,
      origin_data: Sequelize.STRING,
      creator: Sequelize.STRING,
      create_block_num: Sequelize.INTEGER
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'accounts'
    }
  },
  // table account_token
  {
    keys: {
      name: Sequelize.STRING,
      token_type: Sequelize.STRING,
      available: Sequelize.DOUBLE
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'account_token'
    }
  },
  // table token_info
  {
    keys: {
      issuer: Sequelize.STRING,
      max_supply: Sequelize.STRING,
      supply: Sequelize.STRING,
      symbol: Sequelize.STRING,
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'token_info'
    }
  },
  // public_key_account
  {
    keys: {
      name: Sequelize.STRING,
      public_key: Sequelize.STRING,
      perm_name: Sequelize.STRING,
      weight: Sequelize.INTEGER,
      update_block_num: Sequelize.INTEGER,
      is_deleted: Sequelize.BOOLEAN,
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'public_key_account'
    }
  },
  // contract_account_analytic
  {
    keys: {
      contract_name: Sequelize.STRING,
      transaction_num: Sequelize.INTEGER,
      actions: Sequelize.STRING,
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'contract_account_analytic'
    }
  },
  // fixed_vote 
  {
    keys: {
      key                         : Sequelize.STRING,
      bpname                      : Sequelize.STRING,
      voter                       : Sequelize.STRING,
      freeze_block_num            : Sequelize.STRING,
      is_has_quited               : Sequelize.BOOLEAN,
      unfreeze_block_num          : Sequelize.STRING,
      vote                        : Sequelize.STRING,
      votepower                   : Sequelize.STRING,
      votepower_age               : Sequelize.STRING,
      votepower_age_update_height : Sequelize.STRING,
      typ                         : Sequelize.INTEGER, // -1 是活期
      is_deleted                  : Sequelize.BOOLEAN,
    },
    configs:{
      createdAt: false,
      updatedAt: false,
      tableName: 'fixed_vote'
    }
  },
]